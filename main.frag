#version 320 es

precision mediump float;

uniform sampler2D soundTexture;
uniform vec2 resolution;
uniform float dummy;
uniform float time;
uniform vec2 mouse;

out vec4 out_color;

#define R 500.0

void main(void) {
  vec2 coord_uv = gl_FragCoord.xy / resolution;
  vec2 mouse_uv = mouse.xy / resolution;
  
  /* float t = 1.0 - min(length(coord_uv - mouse_uv), R) / R; */

  /* float time2 = time*2.0; */

  /* //  vec4 x = texture(soundTexture, vec2((0.5+coord_uv.x)*512.0, 0.0)); */
  /* vec4 x = texture2D(soundTexture, vec2(coord_uv.x+0.5, 0)).x; */
  /* //  out_color = x; */
  /* out_color = x * vec4( */
  /* 		   (cos(4.0*t*(coord_uv.x + time2)) + 1.0) / 2.0, */
  /* 		   (sin(3.0*t*(coord_uv.y + time)) + 1.0) / 2.0, */
  /* 		   (cos(5.0*t*(coord_uv.x + coord_uv.y + time2)) + 1.0) / 2.0, */
  /* 		   1.0); */

  vec3 c;
  float z = 0.1 * time;
  vec2 uv = gl_FragCoord.xy / resolution;
  vec2 p = uv - 0.5;
  p.x *= resolution.x / resolution.y;
  float l = 0.1 * length(p);
  for (int i = 0; i < 3; i++) {
    z += 0.07;
    uv += p / l * (sin(z) + 1.0) * abs(sin(l * 9.0 - z * 2.0));
    c[i] = 0.01 / length(abs(mod(uv, 1.0) - 0.5));
  }
  float intensity = texture2D(soundTexture, vec2(l, 0.5)).y;
  out_color = vec4(c / l * (0.1+intensity), time);
  //out_color = vec4(c, intensity, intensity, 1.0);

  //out_color = texture2D(soundTexture, vec2(coord_uv.y, 0.5));
}
