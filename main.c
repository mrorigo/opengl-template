#include <assert.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>
#include <math.h>
#include <pthread.h>

#include <GLFW/glfw3.h>


#define DEFAULT_SCREEN_WIDTH 1024
#define DEFAULT_SCREEN_HEIGHT 768
#define MANUAL_TIME_STEP 0.05

#include "glextloader.c"
#include "file.c"
#include "music.c"
#include "fft4g_h.c"
#include "texture.c"

const char *shader_type_as_cstr(GLuint shader)
{
    switch (shader) {
    case GL_VERTEX_SHADER:
        return "GL_VERTEX_SHADER";
    case GL_FRAGMENT_SHADER:
        return "GL_FRAGMENT_SHADER";
    default:
        return "(Unknown)";
    }
}

bool compile_shader_source(const GLchar *source, GLenum shader_type, GLuint *shader)
{
    *shader = glCreateShader(shader_type);
    glShaderSource(*shader, 1, &source, NULL);
    glCompileShader(*shader);

    GLint compiled = 0;
    glGetShaderiv(*shader, GL_COMPILE_STATUS, &compiled);

    if (!compiled) {
        GLchar message[1024];
        GLsizei message_size = 0;
        glGetShaderInfoLog(*shader, sizeof(message), &message_size, message);
        fprintf(stderr, "ERROR: could not compile %s\n", shader_type_as_cstr(shader_type));
        fprintf(stderr, "%.*s\n", message_size, message);
        return false;
    }

    return true;
}

bool compile_shader_file(const char *file_path, GLenum shader_type, GLuint *shader)
{
    char *source = slurp_file(file_path);
    bool ok = compile_shader_source(source, shader_type, shader);
    if (!ok) {
        fprintf(stderr, "ERROR: failed to compile `%s` shader file\n", file_path);
    }
    free(source);
    return ok;
}

bool link_program(GLuint vert_shader, GLuint frag_shader, GLuint *program)
{
    *program = glCreateProgram();

    glAttachShader(*program, vert_shader);
    glAttachShader(*program, frag_shader);
    glLinkProgram(*program);

    GLint linked = 0;
    glGetProgramiv(*program, GL_LINK_STATUS, &linked);
    if (!linked) {
        GLsizei message_size = 0;
        GLchar message[1024];

        glGetProgramInfoLog(*program, sizeof(message), &message_size, message);
        fprintf(stderr, "Program Linking: %.*s\n", message_size, message);
    }

    glDeleteShader(vert_shader);
    glDeleteShader(frag_shader);

    return program;
}

typedef enum {
    DUMMY_UNIFORM = 0,
    RESOLUTION_UNIFORM,
    TIME_UNIFORM,
    MOUSE_UNIFORM,
    SOUNDTEXTURE_UNIFORM,
    COUNT_UNIFORMS
} Uniform;

static_assert(COUNT_UNIFORMS == 5, "Update list of uniform names");
static const char *uniform_names[COUNT_UNIFORMS] = {
    [DUMMY_UNIFORM] = "dummy",
    [RESOLUTION_UNIFORM] = "resolution",
    [TIME_UNIFORM] = "time",
    [MOUSE_UNIFORM] = "mouse",
    [SOUNDTEXTURE_UNIFORM] = "soundTexture"
};

// Global variables (fragile people with CS degree look away)
bool program_failed = false;

double main_time = 0.0;
GLuint main_program = 0;
GLint main_uniforms[COUNT_UNIFORMS];

bool main_pause = false;

bool load_shader_program(const char *vertex_file_path,
                         const char *fragment_file_path,
                         GLuint *program)
{
    GLuint vert = 0;
    if (!compile_shader_file(vertex_file_path, GL_VERTEX_SHADER, &vert)) {
        return false;
    }

    GLuint frag = 0;
    if (!compile_shader_file(fragment_file_path, GL_FRAGMENT_SHADER, &frag)) {
        return false;
    }

    if (!link_program(vert, frag, program)) {
        return false;
    }

    return true;
}

void reload_shaders(void)
{
  glDeleteProgram(main_program);

  program_failed = true;
  glClearColor(1.0f, 0.0f, 0.0f, 1.0f);

  {
    if (!load_shader_program("main.vert", "main.frag", &main_program)) {
      return;
    }
    
    glUseProgram(main_program);

    for (Uniform index = 0; index < COUNT_UNIFORMS; ++index) {
      main_uniforms[index] = glGetUniformLocation(main_program, uniform_names[index]);
      fprintf(stderr, "main_uniforms[%d]: %d\n", index,main_uniforms[index]);
    }
  }
  
  program_failed = false;
  glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
  
  printf("Successfully Reload the Shaders\n");
}

void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    (void) window;
    (void) scancode;
    (void) action;
    (void) mods;

    if (action == GLFW_PRESS) {
        if (key == GLFW_KEY_F5) {
            reload_shaders();
        } else if (key == GLFW_KEY_SPACE) {
            main_pause = !main_pause;
        } else if (key == GLFW_KEY_Q) {
            exit(1);
        }

        if (main_pause) {
            if (key == GLFW_KEY_LEFT) {
                main_time -= MANUAL_TIME_STEP;
            } else if (key == GLFW_KEY_RIGHT) {
                main_time += MANUAL_TIME_STEP;
            }
        }
    }
}

void window_size_callback(GLFWwindow* window, int width, int height)
{
    (void) window;
    glViewport(0, 0, width, height);
}

void MessageCallback(GLenum source,
                     GLenum type,
                     GLuint id,
                     GLenum severity,
                     GLsizei length,
                     const GLchar* message,
                     const void* userParam)
{
    (void) source;
    (void) id;
    (void) length;
    (void) userParam;
    fprintf(stderr, "GL CALLBACK: %s type = 0x%x, severity = 0x%x, message = %s\n",
            (type == GL_DEBUG_TYPE_ERROR ? "** GL ERROR **" : ""),
            type, severity, message);
}

GLuint dctTexture;
unsigned char soundTexture[1024*4];

void * music_thread_main(void *arg) {
  song_t *main_song = song_open("./jungle2.mp3");
  if(main_song == NULL) {
    fprintf(stderr, "ERROR: Failed to open music!");
    return arg;
  }

  // TODO: Barrier for when to start playing
  (void)arg;
  fprintf(stderr, "music_thread_main\n");
  while(main_song != NULL && song_play(main_song)) {


	memcpy(&dstream2, &dstream, sizeof(double)*1152);
	ddct(1024, 1, dstream2);
	unsigned char ov;
	for(int i=0; i < 1024; i++) {
	  int x = (int)((dstream2[i])/32.0);
	  x = x < -127 ? -127 : (x > 127 ? 127 : x);
	  unsigned char v = (soundTexture[i*4 +0] + (x+128)) >> 1;//(x+127) > 255 ? 255 : (x+127);
	  soundTexture[i*4 +0] = v;
	  soundTexture[i*4 +1] = v;
	  soundTexture[i*4 +2] = ov;
	  soundTexture[i*4 +3] = 255-v;
	  ov = (unsigned char)( ((int)ov+(int)v)/2 );
	}

    
    // Song ended
    usleep(10000/60);
  }

  song_close(main_song);

  fprintf(stderr, "music_thread_main ENDED\n");
  return arg;
}


int main()
{
    if (!glfwInit()) {
        fprintf(stderr, "ERROR: could not initialize GLFW\n");
        exit(1);
    }

    GLFWwindow * const window = glfwCreateWindow(
                                    DEFAULT_SCREEN_WIDTH,
                                    DEFAULT_SCREEN_HEIGHT,
                                    "OpenGL Template",
                                    NULL,
                                    NULL);
    if (window == NULL) {
        fprintf(stderr, "ERROR: could not create a window.\n");
        glfwTerminate();
        exit(1);
    }

    glfwMakeContextCurrent(window);

    load_gl_extensions();

    if (glDrawArraysInstanced == NULL) {
        fprintf(stderr, "Support for EXT_draw_instanced is required!\n");
        exit(1);
    }

    if (glDebugMessageCallback != NULL) {
        glEnable(GL_DEBUG_OUTPUT);
        glDebugMessageCallback(MessageCallback, 0);
    }

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glEnable(GL_TEXTURE_2D);

    glfwSetKeyCallback(window, key_callback);
    glfwSetFramebufferSizeCallback(window, window_size_callback);

    pthread_t thread;
    pthread_create(&thread, NULL, music_thread_main, NULL);

    reload_shaders();

    memset(&soundTexture, 0, 1024*4);

    glGenTextures(1, &dctTexture);
    glActiveTexture(GL_TEXTURE0+1);
    glBindTexture(GL_TEXTURE_2D, dctTexture);

    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE );

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 1024, 1, 0, GL_RGBA, GL_UNSIGNED_BYTE, &soundTexture);


    main_time = glfwGetTime();
    double prev_time;
    while (!glfwWindowShouldClose(window)) {
      //glClear(GL_COLOR_BUFFER_BIT);

      //unsigned int r = (max-min);

      /* for(int i=0; i < 48; i+=4) { */
      /* 	unsigned long col = ((soundTexture[i]<<24) | (soundTexture[i+1]<<16) | (soundTexture[i+2]<<8) | soundTexture[i+3]); */
      /* 	fprintf(stderr, "%08lx ", col); */
      /* } */
      /* fprintf(stderr, "\n"); */
      
      if (!program_failed) {

	/* memcpy(&dstream2, &dstream, sizeof(double)*1152); */
	/* ddct(1024, 1, dstream2); */
	/* for(int i=0; i < 1024; i++) { */
	/*   int x = (int)((dstream2[i])/32.0); */
	/*   x = x < -127 ? -127 : (x > 127 ? 127 : x); */
	/*   unsigned char v = x+128;//(x+127) > 255 ? 255 : (x+127); */
	/*   soundTexture[i*4 +0] = v; */
	/*   soundTexture[i*4 +1] = v; */
	/*   soundTexture[i*4 +2] = v; */
	/*   soundTexture[i*4 +3] = 255-v; */
	/* } */
	/* glBindTexture(GL_TEXTURE_2D, dctTexture); */
	/* glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, 1024, 1, GL_RGBA, GL_UNSIGNED_BYTE, &soundTexture); */
	glBindTexture(GL_TEXTURE_2D, dctTexture);
	glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, 1024, 1, GL_RGBA, GL_UNSIGNED_BYTE, &soundTexture);

	//glUniform1f(main_uniforms[DUMMY_UNIFORM], 1.337f);
	static_assert(COUNT_UNIFORMS == 5, "Update the uniform sync");
	glUniform1i(main_uniforms[SOUNDTEXTURE_UNIFORM], dctTexture);

	int width, height;
	glfwGetWindowSize(window, &width, &height);
	glUniform2f(main_uniforms[RESOLUTION_UNIFORM], width, height);
	glUniform1f(main_uniforms[TIME_UNIFORM], main_time);
	double xpos, ypos;
	glfwGetCursorPos(window, &xpos, &ypos);
	glUniform2f(main_uniforms[MOUSE_UNIFORM], xpos, height - ypos);
	glDrawArraysInstanced(GL_TRIANGLE_STRIP, 0, 4, 1);
      }

      glfwSwapBuffers(window);
      glfwPollEvents();
      
      double cur_time = glfwGetTime();
      if (!main_pause) {
	main_time += cur_time - prev_time;
      }
      prev_time = cur_time;
    }
    

    return 0;
}
