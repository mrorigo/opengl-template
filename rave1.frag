#version 320 es

precision mediump float;

uniform sampler2D soundTexture;
uniform vec2 resolution;
uniform float dummy;
uniform float time;
uniform vec2 mouse;

out vec4 out_color;

#define R 500.0

void main(void) {
  vec2 uv = vec2(gl_FragCoord.x / resolution.x, gl_FragCoord.y / resolution.y);
  vec2 mouse_uv = mouse.xy / resolution.y;
  vec2 p1 = vec2(mod(uv.x,0.5) - 0.25, mod(uv.y, 0.5) - 0.25);

  uv.x += sin(time)/15.0+cos(time/2.0)/14.0;
  uv.y += cos(time/3.0)/16.0+cos(time/3.0)/15.0;
  
  vec3 c;
  float z = sin(0.03 * time*cos(time*2.3));
  vec2 p = vec2(p1.x*cos(z)-p1.y*sin(z), p1.y*cos(z)+p1.x*sin(z));
  p.x *= resolution.x / resolution.y;
  float l = 0.6 * length(p) + 0.01;
  float advance =  texture2D(soundTexture, vec2(50.0/1024.0, 0.0)).z +
    texture2D(soundTexture, vec2(52.0/1024.0, 0.0)).z;
  float sound[4];
  sound[0] = (texture2D(soundTexture, vec2(l/12.0, 0.0)).z);
  sound[1] = (texture2D(soundTexture, vec2(l/6.0, 0.0)).z);
  sound[2] = (texture2D(soundTexture, vec2(l/3.0, 0.0)).z);
  sound[3] = (texture2D(soundTexture, vec2(l, 0.0)).z);

  for (int i = 0; i < 3; i++) {
    z += 0.07;
    uv += advance/16.0 + p / l * (sin(z) + 1.5);// * abs(sin(l * 3.0 - z * 4.0));
    c[i] = sin(time)*0.1 + sound[i+1] * 0.1 / length(abs(mod(uv, 1.0) - 0.5));
  }
  float intensity = clamp(0.05+sound[1], 0.05, 0.95);
  float ll = sqrt(uv.x*uv.x + uv.y*uv.y);

  out_color = vec4(c / l * (intensity), time*3.0)  * (ll/128.0) * intensity;
}
