#include <errno.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include "music.h"

// Some helper functions, to be cleaned up in the future
int scale(mad_fixed_t sample) {
     /* round */
     sample += (1L << (MAD_F_FRACBITS - 16));
     /* clip */
     if (sample >= MAD_F_ONE)
         sample = MAD_F_ONE - 1;
     else if (sample < -MAD_F_ONE)
         sample = -MAD_F_ONE;
     /* quantize */
     return sample >> (MAD_F_FRACBITS + 1 - 16);
}

void ddct(int n, int isgn, double *a);
double dstream[1152];
double dstream2[1152];

void output(song_t *song) {
  struct mad_pcm *pcm = &song->mad_synth.pcm;
  register int nsamples = pcm->length;
  mad_fixed_t const *left_ch = pcm->samples[0],
    *right_ch = pcm->samples[1];
  static char stream[1152*4];
  int error;
  if (pcm->channels == 2) {
    while (nsamples--) {
      signed int sample;
      sample = scale(*left_ch++);
      stream[(pcm->length-nsamples)*4 + 0] = ((sample >> 0) & 0xff);
      stream[(pcm->length-nsamples)*4 + 1] = ((sample >> 8) & 0xff);
      double v = sample;
      sample = scale(*right_ch++);
      stream[(pcm->length-nsamples)*4 + 2] = ((sample >> 0) & 0xff);
      stream[(pcm->length-nsamples)*4 + 3] = ((sample >> 8) & 0xff);
      v = (v+sample);
      
      dstream[pcm->length-nsamples] = ((double)v)/512.0;
    }
    
    if (pa_simple_write(song->device, stream, (size_t)1152*4, &error) < 0) {
      fprintf(stderr, "pa_simple_write() failed: %s\n", pa_strerror(error));
      return;
    }
    //printf('pcm->length=%d\n', nsamples);
    //ddct(1024, 1, dstream);
    //memcpy(&dstream2, &dstream, sizeof(double)*1152);

  } else {
    printf("Mono not supported!");
  }
}

song_t *song_open(const char *file_path)
{
  FILE *fp = fopen(file_path, "r");
  if(fp == NULL) {
    fprintf(stderr, "Could not read file `%s`: %s\n", file_path, strerror(errno)); \
    return NULL;
  }

  struct stat metadata;
  int fd = fileno(fp);
  if (fstat(fd, &metadata) < 0) {
    printf("Failed to stat %s\n", file_path);
    fclose(fp);
    return NULL;
  }
  
  song_t *song = malloc(sizeof(song_t));

  // Set up PulseAudio 16-bit 44.1kHz stereo output
  int error;
  static const pa_sample_spec ss = { .format = PA_SAMPLE_S16LE, .rate = 44100, .channels = 2 };
  if (!(song->device = pa_simple_new(NULL, "Demo MP3 player", PA_STREAM_PLAYBACK, NULL, "playback", &ss, NULL, NULL, &error))) {
    fprintf(stderr, "pa_simple_new() failed\n");
    return false;
  }

  song->fd = fd;
  song->input_stream = mmap(0,
			    metadata.st_size,
			    PROT_READ,
			    MAP_SHARED,
			    fd,
			    0);

  mad_stream_buffer(&song->mad_stream,
		    song->input_stream,
		    metadata.st_size);

  return song;
}

bool song_play(song_t *song)
{
  if(song == NULL) {
    fprintf(stderr, "ERROR: music: song_lay with NULL song\n");
    return false;
  }

  // Decode frame from the stream
  if (mad_frame_decode(&song->mad_frame, &song->mad_stream)) {
    if (MAD_RECOVERABLE(song->mad_stream.error)) {
      return true;
    } else if (song->mad_stream.error == MAD_ERROR_BUFLEN) {
      return true;
    } else {
      fprintf(stderr, "ERROR: music: mad_frame_decode failed: error=%d\n", song->mad_stream.error);
      return false;
    }
  }
  
  // Synthesize PCM data of frame
  mad_synth_frame(&song->mad_synth, &song->mad_frame);
  output(song);

  return true;
}

void song_close(song_t *song)
{
  mad_synth_finish(&song->mad_synth);
  mad_frame_finish(&song->mad_frame);
  mad_stream_finish(&song->mad_stream);
  if(song->device != NULL) {
    pa_simple_free(song->device);
  }

  close(song->fd);
}

