#ifndef __MUSIC_H__
#define __MUSIC_H__
#include <stdbool.h>
#include <mad.h>
#include <pulse/simple.h>
#include <pulse/error.h>

typedef struct _song_t {
  int fd;
  unsigned char *input_stream; // mmap:ed
  struct mad_stream mad_stream;
  struct mad_frame mad_frame;
  struct mad_synth mad_synth;
  pa_simple *device;
  volatile unsigned int frame;
} song_t;

song_t *song_open(const char *file_path);
bool song_play(song_t *song);
void song_close(song_t *song);
#endif
