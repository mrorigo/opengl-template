#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"
#include <GL/gl.h>

typedef struct _texture_t {
  int width;
  int height;
  int nrChannels;
  unsigned char *data;
  unsigned int glTexture;
} texture_t;

texture_t *texture_load(const char *filename) {

  texture_t *texture = malloc(sizeof(texture_t));
  
  texture->data = stbi_load(filename, &texture->width, &texture->height, &texture->nrChannels, 0);
  if(texture->data == NULL) {
    free(texture);
    return NULL;
  }
  glGenTextures(1, &texture->glTexture);
  glBindTexture(GL_TEXTURE_2D, texture->glTexture);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, texture->width, texture->height, 0, texture->nrChannels == 3 ? GL_RGB : GL_RGBA, GL_UNSIGNED_BYTE, texture->data);

  return texture;
}

void texture_free(texture_t *texture) {
  stbi_image_free(texture->data);
  free(texture);
}
