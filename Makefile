PKGS=glfw3 gl mad libpulse libpulse-simple
CFLAGS=-Wall -Wextra -ggdb

main: main.o
	$(CC) $(CFLAGS) -o main main.o `pkg-config --libs $(PKGS)` -lm -lpthread

main.o: main.c file.h
	$(CC) $(CFLAGS) `pkg-config --cflags $(PKGS)` -c main.c
